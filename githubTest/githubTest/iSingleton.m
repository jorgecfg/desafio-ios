//
//  iSingleton.m
//  githubTest
//
//  Created by Jorge Luis Beckel Flor on 05/01/17.
//  Copyright © 2017 Jorge Luis Beckel Flor. All rights reserved.
//

#import "iSingleton.h"

@implementation iSingleton

+ (id)sharedManager {
    static iSingleton *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    if (self = [super init]) {
    }
    return self;
}


@end
