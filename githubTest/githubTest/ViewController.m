//
//  ViewController.m
//  githubTest
//
//  Created by Jorge Luis Beckel Flor on 03/01/17.
//  Copyright © 2017 Jorge Luis Beckel Flor. All rights reserved.
//

#import "ViewController.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "UIRefreshControl+AFNetworking.h"
#import "UIActivityIndicatorView+AFNetworking.h"
#import "UIScrollView+InfiniteScroll.h"


@interface ViewController ()

@end

@implementation ViewController
NSMutableArray *objData;
UIRefreshControl *refreshControl;
UIActivityIndicatorView *activityIndicator;
int PaginaAtual;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    /// Titulo
    [self setTitle:@"Lista Repositórios para Java"];
    
    /// Define StatusBar Branca
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    /// Activity
    activityIndicator = [[UIActivityIndicatorView alloc] init];
    [activityIndicator setColor:[UIColor blackColor]];
    [activityIndicator setCenter:self.view.center];
    [self.view addSubview:activityIndicator];
    
    /// Refresh Control
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(carregaDados) forControlEvents:UIControlEventValueChanged];
    [_tbCentral addSubview:refreshControl];
    
    /// Inicializa Variáveis
    PaginaAtual = 1;
    
    /// Configura EndLess Scroll
    [_tbCentral addInfiniteScrollWithHandler:^(UITableView *tableView){
       
        /// Incrementa Página
        PaginaAtual++;
        
        /// Chama Dados
        [self carregaDados];
        
        /// fechamento
        [tableView finishInfiniteScroll];
    }];
    
    
    /// Carrega dados, se existir dados offline
    [self carregaDados];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - CarregaDados
- (void)carregaDados
{
    
    /// mostra activity
    [activityIndicator startAnimating];
    
    
    /// Chamada
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    /// Serviço
    /// Url para envio, com a variável de página
    NSString *apiURL = [NSString stringWithFormat:@"https://api.github.com/search/repositories?q=language:Java&sort=stars&page=%d",PaginaAtual];
    NSLog(@"apiURL: %@", apiURL);
    
    [manager GET:apiURL parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        
        NSLog(@"Response: %@", responseObject);

        
        /// Le registros
        [self LeRegistros:responseObject];
        
        
        
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
    {
        NSLog(@"error: %@", error.description);
        NSLog(@"error: %@", error.localizedDescription);
        NSLog(@"error: %@", error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]);
    }];
    
    
    
    // Finaliza refresh
    [refreshControl endRefreshing];
    
    /// Para de mostrar indicador
    [activityIndicator stopAnimating];
    
}


- (void) LeRegistros:(NSMutableDictionary *)responseObject
{
    /// Inicializa obj DataSource, se estiver 0
    if (objData.count == 0) {
        objData = [[NSMutableArray alloc] init];
    }
    
    /// Pega árvore dos Items
    NSMutableDictionary *ObjDicRaiz = [responseObject objectForKey:@"items"];
    
    /// Percorre resultados e adiciona no Obj DataSorce
    for (NSDictionary* ObjDic in ObjDicRaiz) {
    
        NSLog(@"ObjDic: %@", ObjDic);
        
        /// Verifica se retornou nill
        if (ObjDic == nil) {
            NSLog(@"Sem Dados");
            
            
            UILabel *SemDados = [[UILabel alloc] initWithFrame:CGRectMake(00.0f, 120.0f, self.view.frame.size.width, 35.0f)];
            [SemDados setTextAlignment:NSTextAlignmentCenter];
            [SemDados setText:@"Nenhuma Repositório"];
            [SemDados setTag:864];
            [self.view addSubview:SemDados];
        }
        else
        {
            
            /// Add no Objeto Data
            [objData addObject:ObjDic];
            
        }
        
        
        
        
        
    }
    
    
    /// Reload Data
    [_tbCentral reloadData];
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    /// Total Registros
    NSLog(@"TOTAL: %lu", (unsigned long)objData.count);
    return objData.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    /// Carrega a cell customizada
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellList" forIndexPath:indexPath];
    
    /// Pega o Dictionary da linha atual
    NSDictionary *status = (NSDictionary *)[objData objectAtIndex:indexPath.row];
    NSDictionary *statusOwner = (NSDictionary *)[status objectForKey:@"owner"];
    NSLog(@"Status: %@", status);
    
    
    /// Log
    NSLog(@"Descrição: %@", [status objectForKey:@"description"]);
    NSLog(@"Name: %@", [status objectForKey:@"name"]);
    NSLog(@"ID: %@", [status objectForKey:@"id"]);
    NSLog(@"forks_count: %@", [status objectForKey:@"forks_count"]);
    NSLog(@"stargazers_count: %@", [status objectForKey:@"stargazers_count"]);
    
    /// Login GitHub
    NSString *usernameGithub = [statusOwner objectForKey:@"login"];
    
    /// Nome Completo usuário
    NSString *NomeCompleto = [self FullNameUser:usernameGithub];
    
    /// Campos
    [self SetFieldText:self.view :1001 :[status objectForKey:@"name"]]; // Nome do Repositório
    [self SetFieldText:self.view :1002 :[status objectForKey:@"description"]]; // Descrição
    [self SetFieldImage:self.view :1003 :[statusOwner objectForKey:@"avatar_url"]]; // Nome do Repositório
    [self SetFieldText:self.view :1004 :usernameGithub]; // Username
    [self SetFieldText:self.view :1005 :NomeCompleto]; // Nome Completo
    [self SetFieldText:self.view :1006 :[self FormatNumberMilion:[status objectForKey:@"forks_count"]]]; // Fork
    [self SetFieldText:self.view :1007 :[self FormatNumberMilion:[status objectForKey:@"stargazers_count"]]]; // Favorito
    
    /// Cor Linha
    UIColor *corLinha = [UIColor colorWithRed:121/255.0 green:147/255.0 blue:158/255.0 alpha:1];
    
    /// Adiciona linha no bottom
    UIView *vwLinhaBottom = [[UIView alloc] initWithFrame:CGRectMake(0,cell.frame.size.height - 2, cell.frame.size.width, 1)];
    [vwLinhaBottom setBackgroundColor:corLinha];
    [cell addSubview:vwLinhaBottom];
    
    
    /// Retorna célula
    return cell;
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /// Desseleciona
    [_tbCentral deselectRowAtIndexPath:indexPath animated:NO];
    
    /// Dados
    NSDictionary *status = (NSDictionary *)[objData objectAtIndex:indexPath.row];
    
    /// Guarda idPromocao
    [self.iSingleton setObjDetalheRepositorio:status];
    
    /// Redireciona para inicial
    UIViewController *toViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"detalheRepositorio"];
    [self.navigationController pushViewController:toViewController animated:YES];
    
    /// Abre Detalhes
    //    [self MostraDetalhes:indexPath];
}



@end
