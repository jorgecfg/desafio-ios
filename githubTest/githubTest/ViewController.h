//
//  ViewController.h
//  githubTest
//
//  Created by Jorge Luis Beckel Flor on 03/01/17.
//  Copyright © 2017 Jorge Luis Beckel Flor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "viewControllerPadrao.h"

@interface ViewController : viewControllerPadrao <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tbCentral;


@end

