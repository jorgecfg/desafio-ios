//
//  iSingleton.h
//  githubTest
//
//  Created by Jorge Luis Beckel Flor on 05/01/17.
//  Copyright © 2017 Jorge Luis Beckel Flor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface iSingleton : NSObject

/// Veriáveis
@property (nonatomic, strong) NSDictionary *objDetalheRepositorio;


/// Métodos Não Apagaveis
+ (id)sharedManager;

@end
