//
//  viewControllerPadrao.m
//  githubTest
//
//  Created by Jorge Luis Beckel Flor on 05/01/17.
//  Copyright © 2017 Jorge Luis Beckel Flor. All rights reserved.
//

#import "viewControllerPadrao.h"
#import "UIImageView+AFNetworking.h"
#import "AFNetworking.h"


@interface viewControllerPadrao ()


@end

@implementation viewControllerPadrao


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    /// Atualiza valores de largura e altura
    [self setWidth:[UIScreen mainScreen].bounds.size.width];
    [self setHeight:[UIScreen mainScreen].bounds.size.height];
    
    /// Carrega SIngleton
    iSingleton *Sg = [iSingleton sharedManager];
    [self setISingleton:Sg];

}

- (void)viewDidAppear:(BOOL)animated
{
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - UserDefaults
- (void) UDRemove:(NSString *)ObjKey
{
    NSUserDefaults *xpto = [NSUserDefaults standardUserDefaults];
    [xpto removeObjectForKey:ObjKey];
}

- (void) UDSet:(NSString *)ObjKey :(NSObject *)ObjValue
{
    NSUserDefaults *xpto = [NSUserDefaults standardUserDefaults];
    [xpto setObject:ObjValue forKey:ObjKey];
}

- (NSObject *)UDGet:(NSString *)ObjKey
{
    NSUserDefaults *xpto = [NSUserDefaults standardUserDefaults];
    return [xpto objectForKey:ObjKey];
}

- (NSString *)UDGetString:(NSString *)ObjKey
{
    NSUserDefaults *xpto = [NSUserDefaults standardUserDefaults];
    return [NSString stringWithFormat:@"%@",[xpto objectForKey:ObjKey]];
}

#pragma mark - Funcoes
- (NSString *) FormatNumberMilion:(NSString *)TotalValor
{
    /// Converte TotalValor para Int
    /// Assim é mais fácil de trabalhar com a chegada e não fica código redundante
    int Total = [TotalValor intValue];
    
    /// Variável de Saída, com valor padrão
    NSString *valorTexto = [NSString stringWithFormat:@"%d", Total];
    
    /// Inicializa try...catch
    @try {
        /// Verifica se valor é acima do Milhar
        if (Total > 999)
        {
            /// Calculo
            int TotalK = Total / 1000;
            
            /// String
            valorTexto = [NSString stringWithFormat:@"%dK", TotalK];
        }

        /// Verifica se valor é acima do Milhão
        if (Total > 999999)
        {
            /// Calculo
            int TotalM = Total / 1000000;
            
            /// String
            valorTexto = [NSString stringWithFormat:@"%dM", TotalM];
        }
        
        
        
    } @catch (NSException *exception) {
        /// Erro
        /// Retorna valor padrão
        
    } @finally {
        /// Retorno
        return valorTexto;
    }
    
}

- (NSString *) FullNameUser:(NSString *)Username
{
    /// Retorno do Nome, padrão é o username
    __block NSString *NomeCompleto = Username;
    
    /// Variável do UserDefaults
    NSString *NomeCompletoUD = [self UDGetString:Username];
    
    /// Verifica se já existe Cache (userdefaults) para este username
    if ([self UDGet:Username] != nil) {
        /// Tem Cache, Salva a variável em NomeCompleto
        NomeCompleto = NomeCompletoUD;
    }
    else
    {
        /// Não tem cache
        /// Chamada
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        
        /// Serviço
        /// Url para envio
        NSString *apiURL = [NSString stringWithFormat:@"https://api.github.com/users/%@",Username];
        NSLog(@"apiURL Full Name: %@", apiURL);
        
        [manager GET:apiURL parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            /// Log
            NSLog(@"Response FullName: %@", responseObject);
            
            /// Coleta o Nome completo
            NomeCompleto = [responseObject objectForKey:@"name"];

            /// Se nome completo for Null, usa o Login
            if([NomeCompleto isKindOfClass:[NSNull class]])
            {
                NomeCompleto = Username;
            }
            
            // CACHE SALVA, via username
            [self UDSet:Username : NomeCompleto];
            
            
            
            
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
         {
             NSLog(@"error: %@", error.description);
             NSLog(@"error: %@", error.localizedDescription);
             NSLog(@"error: %@", error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]);
         }];
    }
    
    /// Retorno
    return NomeCompleto;
}


#pragma mark - Campos Tela
- (void) SetFieldText:(UIView *)vwOriginal :(int)TagNum :(NSString *)TextValue
{
    /// Get Label
    UILabel *lbTest = (UILabel *)[vwOriginal viewWithTag:TagNum];
    
    /// Forçar ser String... Mesmo que venha um número como String, será forçado ser string
    [lbTest setText:[NSString stringWithFormat:@"%@",TextValue]];
}

- (void) SetFieldImage:(UIView *)vwOriginal :(int)TagNum :(NSString *)UrlImagem
{
    /// Image PlaceHolder
    UIImage *imagePlaceHolder = [UIImage imageNamed:@"team-placeholder.png"];
    
    /// URL Image
    NSURL *urlImagem = [NSURL URLWithString:UrlImagem];
    
    /// Get Label
    UIImageView *ivTest = (UIImageView *)[vwOriginal viewWithTag:TagNum];
    [ivTest setImageWithURL:urlImagem placeholderImage:imagePlaceHolder];
}


@end
