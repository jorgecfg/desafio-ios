//
//  viewControllerPadrao.h
//  githubTest
//
//  Created by Jorge Luis Beckel Flor on 05/01/17.
//  Copyright © 2017 Jorge Luis Beckel Flor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iSingleton.h"

@interface viewControllerPadrao : UIViewController <UIApplicationDelegate, UITextFieldDelegate>

#pragma mark - Variáveis
@property (nonatomic, retain) IBOutlet iSingleton *iSingleton;
@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) CGFloat height;

#pragma mark - Main
- (void) LoadSaveMainConfig;

#pragma mark - UserDefaults
- (void) UDRemove:(NSString *)ObjKey;
- (void) UDSet:(NSString *)ObjKey :(NSObject *)ObjValue;
- (NSObject *) UDGet:(NSString *)ObjKey;
- (NSString *)UDGetString:(NSString *)ObjKey;

#pragma mark - Funcoes
- (NSString *) FormatNumberMilion:(NSString *)Total;
- (NSString *) FullNameUser:(NSString *)Username;

#pragma mark - Campos Tela
- (void) SetFieldText:(UIView *)vwOriginal :(int)TagNum :(NSString *)TextValue;
- (void) SetFieldImage:(UIView *)vwOriginal :(int)TagNum :(NSString *)UrlImagem;

@end
