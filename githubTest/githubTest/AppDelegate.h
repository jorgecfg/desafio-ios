//
//  AppDelegate.h
//  githubTest
//
//  Created by Jorge Luis Beckel Flor on 03/01/17.
//  Copyright © 2017 Jorge Luis Beckel Flor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

