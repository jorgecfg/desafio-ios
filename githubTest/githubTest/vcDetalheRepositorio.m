//
//  ViewController.m
//  githubTest
//
//  Created by Jorge Luis Beckel Flor on 03/01/17.
//  Copyright © 2017 Jorge Luis Beckel Flor. All rights reserved.
//

#import "vcDetalheRepositorio.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "UIRefreshControl+AFNetworking.h"
#import "UIActivityIndicatorView+AFNetworking.h"
#import "UIScrollView+InfiniteScroll.h"


@interface vcDetalheRepositorio ()

@end

@implementation vcDetalheRepositorio
NSMutableArray *objDataRep;
UIRefreshControl *refreshControlRep;
NSDictionary *DadosRep;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    /// Tint Back
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    
    /// Refresh Control
    refreshControlRep = [[UIRefreshControl alloc] init];
    [refreshControlRep addTarget:self action:@selector(carregaDados) forControlEvents:UIControlEventValueChanged];
    [_tbCentral addSubview:refreshControlRep];
    
    
    /// Carrega dados, se existir dados offline
    [self carregaDados];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - CarregaDados
- (void)carregaDados
{
    /// Dados do Repositorio Selecionado
    DadosRep = [[NSDictionary alloc] initWithDictionary:self.iSingleton.objDetalheRepositorio];

    /// Titulo
    [self setTitle:[DadosRep objectForKey:@"name"]];

    
    /// Chamada
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    /// URL Pulls
    /// Remove o "{/number}" ao final
    NSString *urlPulls = [DadosRep objectForKey:@"pulls_url"];
    urlPulls = [urlPulls stringByReplacingOccurrencesOfString:@"{/number}" withString:@""];
    
    /// Serviço
    /// Url para envio, com a variável de página
    NSString *apiURL = urlPulls;
    NSLog(@"apiURL: %@", apiURL);
    
    [manager GET:apiURL parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        
        NSLog(@"Response: %@", responseObject);
        
        
        /// Le registros
        [self LeRegistros:responseObject];
        
        
        
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
     {
         NSLog(@"error: %@", error.description);
         NSLog(@"error: %@", error.localizedDescription);
         NSLog(@"error: %@", error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]);
     }];
    
    
    
    // Finaliza refresh
    [refreshControlRep endRefreshing];
    
}


- (void) LeRegistros:(NSMutableDictionary *)responseObject
{
    /// Inicializa obj DataSourceRep
    objDataRep = [[NSMutableArray alloc] init];
    
    /// Percorre resultados e adiciona no Obj DataSorce
    for (NSDictionary* ObjDic in responseObject) {
        
        NSLog(@"ObjDic: %@", ObjDic);
        
        /// Verifica se retornou nill
        if (ObjDic == nil) {
            NSLog(@"Sem Dados");
            
            
            UILabel *SemDados = [[UILabel alloc] initWithFrame:CGRectMake(00.0f, 120.0f, self.view.frame.size.width, 35.0f)];
            [SemDados setTextAlignment:NSTextAlignmentCenter];
            [SemDados setText:@"Nenhuma Pull Request"];
            [SemDados setTag:864];
            [self.view addSubview:SemDados];
        }
        else
        {
            
            /// Add no Objeto Data
            [objDataRep addObject:ObjDic];
            
        }
        
        
        
        
        
    }
    
    
    /// Reload Data
    [_tbCentral reloadData];
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    /// Total Registros
    NSLog(@"TOTAL: %lu", (unsigned long)objDataRep.count);
    return objDataRep.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    /// Carrega a cell customizada
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellListRep" forIndexPath:indexPath];
    
    /// Pega o Dictionary da linha atual
    NSDictionary *status = (NSDictionary *)[objDataRep objectAtIndex:indexPath.row];
    NSDictionary *statusOwner = (NSDictionary *)[status objectForKey:@"user"];
    NSLog(@"Status: %@", status);
    
    /// Login GitHub
    NSString *usernameGithub = [statusOwner objectForKey:@"login"];
    
    /// Nome Completo usuário
    NSString *NomeCompleto = [self FullNameUser:usernameGithub];
    
    /// Campos
    [self SetFieldText:self.view :1001 :[status objectForKey:@"title"]]; // Nome do Repositório
    [self SetFieldText:self.view :1002 :[status objectForKey:@"body"]]; // Descrição
    [self SetFieldImage:self.view :1003 :[statusOwner objectForKey:@"avatar_url"]]; // Nome do Repositório
    [self SetFieldText:self.view :1004 :usernameGithub]; // Username
    [self SetFieldText:self.view :1005 :NomeCompleto]; // Nome Completo
    
    /// Cor Linha
    UIColor *corLinha = [UIColor colorWithRed:121/255.0 green:147/255.0 blue:158/255.0 alpha:1];
    
    /// Adiciona linha no bottom
    UIView *vwLinhaBottom = [[UIView alloc] initWithFrame:CGRectMake(0,cell.frame.size.height - 2, cell.frame.size.width, 1)];
    [vwLinhaBottom setBackgroundColor:corLinha];
    [cell addSubview:vwLinhaBottom];
    
    
    /// Retorna célula
    return cell;
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /// Desseleciona
    [_tbCentral deselectRowAtIndexPath:indexPath animated:NO];
    
    /// Dados
    NSDictionary *status = (NSDictionary *)[objDataRep objectAtIndex:indexPath.row];
    
    /// Guarda idPromocao
    [self.iSingleton setObjDetalheRepositorio:status];
    
    /// Redireciona para inicial
    UIViewController *toViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"detalheRepositorio"];
    [self.navigationController pushViewController:toViewController animated:YES];
    
    /// Abre Detalhes
    //    [self MostraDetalhes:indexPath];
}



@end
